# Ansible scripts to setup wordpress and monitoring server

#### Prerequisites
  - Ansible 2.4
  - python zabbix-api in the ansible servers or host running this ansible scripts. Simply run `pip install zabbix-api` to install the module
  - Terraform (optional) if you need to auto provision the aws instances

## 1. Provision AWS instances with terraform (OPTIONAL)
Step no.1 is optional if you need to provision aws instances for your test environment. Alternatively if you already have your test instances, just skip this step and go to [step no.2](https://gitlab.com/interview-test/wordpress-monitoring#2-wordpress-and-monitoring-server-deployment) to run the ansible setup scripts.

###### a. Update terraform variables
There are several variable that you might need to update in `variables.tf` based on your local environment setup
* `public_key_path` set this to your public key path
* `key_name` unique key pair identifier name
* `domain` private hosted domain for internal hostname
* `instance_type` set to the instance_type that you will use to run the instance

###### b. Configure AWS access and secret key
Before we start, configure the aws secret key and access key
```
# export AWS_ACCESS_KEY_ID="YourAccessKey"
# export AWS_SECRET_ACCESS_KEY="YourSecretKey"

```

##### c. Run terraform!!!!
Use terraform apply to run all the instances provisioning
```
# sh terraform_provision.sh
```

When the instances provisioning are completed, you will see such output displayed on your screen:
```
Apply complete! Resources: 19 added, 0 changed, 0 destroyed.

Outputs:

sysmon hostname = sysmon.example.com
sysmon private_ip = 10.10.1.214
sysmon public_ip = 34.229.216.211
wordpress hostname = wordpress.example.com
wordpress private_ip = 10.10.1.105
wordpress public_ip = 54.198.246.17
```

Please take note of the `sysmon public_ip` and `wordpress public_ip` since this two information will be the one used for ansible inventory later on. So we can easily summarize the output as shown below:
```
wordpress.example.com         54.198.246.17
sysmon.example.com        34.229.216.211
```

## 2. Wordpress and monitoring server deployment
##### a. Install ansible roles
Below are the list of all the required ansible roles and its respective github repositories:
* https://github.com/dj-wasabi/ansible-zabbix-agent
* https://github.com/dj-wasabi/ansible-zabbix-web
* https://github.com/dj-wasabi/ansible-zabbix-server
* https://github.com/geerlingguy/ansible-role-mysql
* https://gitlab.com/interview-test/ansible-common-package
* https://gitlab.com/interview-test/ansible-docker-wordpress

Install all the roles using `ansible-galaxy`
```
# ansible-galaxy install -r requirements.yml -p /etc/ansible/roles/
```

##### b. Update ansible inventory
Update your ansible inventory to use the correct `ansible_host` and make sure the FQDN also match the IP address. If your server fqdn can be resolved by public/internal dns, just remove the `ansible_host` configuration
```
[monitoring-servers]
sysmon.example.com    ansible_user=centos ansible_host=34.229.216.211

[wordpress-servers]
server.example.com   ansible_user=centos ansible_host=54.198.246.17

[servers:children]
wordpress-servers

```

##### c. Update ansible group vars
Update ansible groups vars to define the zabbix_url and also the zabbix server private ip address
`all.yml`
```
zabbix_agent_server: 10.10.1.214
zabbix_agent_serveractive: 10.10.1.214
zabbix_url: http://34.229.216.211

```

##### d. Run ansible playbook
And finally, Run the ansible playbook:
```
# ansible-playbook -i hosts playbook.yml
```
Ansible will perform the following tasks:

   - Install basic package and zabbix agent on all servers
   - Install docker and configure container for mysql, nginx and wordpress
   - Install zabbix server and services on sysmon
   - Automatically add the server to "Linux Servers" hostgroups and setup basic monitoring such as cpu, disk, memory

##### 3. That's it!!! Now you have the wordpress and monitoring server
Default creds for zabbix server:
```
URL: http://[zabbix public ip]
username: Admin
password: zabbix
```

#### Known Issues and Future Improvement:
None as of now
