resource "aws_instance" "wordpress" {

  instance_type = "${var.instance_type}"
  ami = "${lookup(var.aws_amis, var.aws_region)}"
  key_name = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = ["${aws_default_security_group.default.id}","${aws_security_group.default.id}"]
  subnet_id = "${aws_subnet.my_subnet.0.id}"

  tags {
    Name = "wordpress"
  }

  root_block_device {
    delete_on_termination = true
  }

  provisioner "local-exec" {
    command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible all -i ${aws_instance.wordpress.public_ip}, -m hostname -a 'name=${aws_instance.wordpress.tags.Name}.${aws_route53_zone.default.name}' -b -u centos"
  }

}

resource "aws_instance" "sysmon" {

  instance_type = "${var.instance_type}"
  ami = "${lookup(var.aws_amis, var.aws_region)}"
  key_name = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = ["${aws_default_security_group.default.id}","${aws_security_group.default.id}"]
  subnet_id = "${aws_subnet.my_subnet.0.id}"

  tags {
    Name = "sysmon"
  }

  root_block_device {
    delete_on_termination = true
  }

  provisioner "local-exec" {
    command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible all -i ${aws_instance.sysmon.public_ip}, -m hostname -a 'name=${aws_instance.sysmon.tags.Name}.${aws_route53_zone.default.name}' -b -u centos"
  }

}
