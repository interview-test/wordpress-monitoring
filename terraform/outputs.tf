output "wordpress public_ip" {
   value = "${aws_instance.wordpress.public_ip}"
}

output "sysmon public_ip" {
   value = "${aws_instance.sysmon.public_ip}"
}

output "wordpress private_ip" {
   value = "${aws_instance.wordpress.private_ip}"
}

output "sysmon private_ip" {
   value = "${aws_instance.sysmon.private_ip}"
}

output "wordpress hostname" {
   value = "${aws_route53_record.wordpress.fqdn}"
}

output "sysmon hostname" {
   value = "${aws_route53_record.sysmon.fqdn}"
}
