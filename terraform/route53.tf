resource "aws_route53_zone" "default" {
  name = "${var.domain}"
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route53_record" "wordpress" {
  zone_id = "${aws_route53_zone.default.zone_id}"
  name = "wordpress"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.wordpress.private_ip}"]
}

resource "aws_route53_record" "sysmon" {
  zone_id = "${aws_route53_zone.default.zone_id}"
  name = "sysmon"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.sysmon.private_ip}"]
}
