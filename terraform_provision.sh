#!/bin/bash

TERRAFORM_DIR="`pwd`/terraform"

if ! command -v terraform > /dev/null; then
  echo "No terraform executables found. Please install terraform first!!!"
  exit 1
fi

if [ ! -d .terraform ]; then
    terraform init $TERRAFORM_DIR
fi

echo "Run terraform apply"
terraform apply $TERRAFORM_DIR
